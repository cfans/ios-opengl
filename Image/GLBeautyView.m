//
//  GLBeautyView.m
//  GLGraph
//
//  Created by cfans on 2017/12/12.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLBeautyView.h"

static NSString *vertexShaderSource = ShaderCode(
     attribute vec4 position;
     attribute vec2 texCoordIn;
     varying lowp vec2 textureCoordinate;

     void main() {
         textureCoordinate = texCoordIn;
         gl_Position = position;
     }
);

static NSString *fragmentShaderSource = ShaderCode(
    precision highp float;
    varying highp vec2 textureCoordinate;
    uniform sampler2D vTexture;
);


@implementation GLBeautyView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
