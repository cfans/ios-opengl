//
//  GLEyeView.m
//  GLGraph
//
//  Created by cfans on 2017/12/12.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLEyeView.h"
@import OpenGLES;
#import <GLKit/GLKit.h>



static NSString *vertexShaderSource = ShaderCode(
     precision highp float;
     attribute vec4 position;
     attribute vec2 texturecoord;
     attribute vec3 normal;

     varying lowp vec4 colorVarying;
     varying lowp vec2 v_texCoord;

     uniform mat4 projectionMatrix;
     uniform mat3 modelViewMatrix;

     void main() {

         vec3 eyeNormal = normalize(modelViewMatrix * normal);
//         eyeNormal = vec3(1.0, 1.0, 1.0);
         vec3 lightPosition = vec3(0.0, 0.0, 1.0);
         vec4 diffuseColor = vec4(0.4, 0.4, 1.0, 1.0);

         float nDotVP = max(0.0, dot(eyeNormal, normalize(lightPosition)));
         colorVarying = diffuseColor * nDotVP;
         v_texCoord = texturecoord;
         vec3 npos = normalize(position.xyz);
         vec2 px = npos.xz;
         float sinthita = length(px);
         px = normalize(px);

       float thita = asin(sinthita);
       float py=2.0;
       v_texCoord=px*py*(600.0)*0.5+0.5;

//         v_texCoord=texturecoord;
         gl_Position = position;

     }
);

static NSString *fragmentShaderSource = ShaderCode(
   uniform sampler2D textureImage;
   varying lowp vec2 v_texCoord;

   void main() {
       mediump vec4 RGBA;
       RGBA = texture2D(textureImage, v_texCoord);
       gl_FragColor = RGBA ;
   }
);

//static NSString *fragmentShaderSource = ShaderCode(
//
//   precision highp float;
//   varying lowp vec2 texCoordOut;
//
//   uniform sampler2D textureImage;
//   uniform mat4 texcoordMatrix;
//   uniform float aperture;
//
//
//   const float PI = 3.1415926535;
//
//   void main() {
//
//       vec4 vertColor = texture2D(textureImage, texCoordOut);
//       vec4 vertTexcoord = texture2D(textureImage, texCoordOut);
//
//
//       float apertureHalf = 0.5 * 0.5 * (PI / 180.0);
//       float maxFactor = sin(apertureHalf);
//       vec2 stFactor = vec2(1.0 / abs(texcoordMatrix[0][0]), 1.0 / abs(texcoordMatrix[1][1]));
//       vec2 pos = (2.0 * vertTexcoord.st * stFactor - 1.0);
//
//       float l = length(pos);
//       if (l > 1.0)
//       {
//           gl_FragColor = vec4(0, 0, 0, 1);
//       }
//       else
//       {
//           float x = maxFactor * pos.x;
//           float y = maxFactor * pos.y;
//
//           float n = length(vec2(x, y));
//
//           float z = sqrt(1.0 - n * n);
//
//           float r = atan(n, z) / PI;
//
//           float phi = atan(y, x);
//
//           float u = r * cos(phi) + 0.5;
//           float v = r * sin(phi) + 0.5;
//
//           gl_FragColor = texture2D(textureImage, vec2(u, v) / stFactor ) * vertColor;
//       }
//   }
//);

typedef struct
{
    float position[4];
    float textureCoordinate[2];
} Vertex;

enum
{
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_INPUT_TEXTURE_COORDINATE,
    ATTRIBUTE_NORMAL,
    NUM_ATTRIBUTES
};
GLint glAttribute[NUM_ATTRIBUTES];

enum
{
    UNIFORM_PROJECTION_MARTRIX,
    UNIFORM_MODELVIEW_MARTRIX,
    NUM_UNIFORMS
};
GLint uniforms[NUM_UNIFORMS];


@interface GLEyeView(){
    GLuint       indexBuffer;
    GLuint       vertexBuffer;
    GLuint       _texture;
    GLuint       _programHandle;
}
@end;

@implementation GLEyeView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupGLView];
    }
    return self;
}


-(void)setupGLView{
    [super setupGLView];
    [self compileShaders];
    [self setupVBOs];
}

-(void)setImage:(UIImage *)image{
    [self renderImage:image];
    [self render];

}

- (void)setupVBOs {
    static const Vertex vertices[] =
    {
        { .position = { 1.0, -1.0,0, 1}, .textureCoordinate = { 1.0, 0.0 } },
        { .position = {  1.0, 1.0,0, 1 }, .textureCoordinate = { 1.0, 1.0 } },
        { .position = { -1.0,  1.0,0, 1 }, .textureCoordinate = { 0.0, 1.0 } },
        { .position = {  -1.0,  -1.0,0, 1 }, .textureCoordinate = { 0, 0 } }
    };

    static const GLubyte index[] =
    {
        0, 1, 2,
        2, 3, 0
    };

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index,GL_STATIC_DRAW);

    glVertexAttribPointer(glAttribute[ATTRIBUTE_POSITION], 4, GL_FLOAT, GL_FALSE,  sizeof(Vertex), (void*)0);

    glVertexAttribPointer(glAttribute[ATTRIBUTE_INPUT_TEXTURE_COORDINATE], 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(4 * sizeof(float)));

}

- (void)compileShaders {
    GLuint vertexShader = [GLBaseView compileShaderCode:vertexShaderSource withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [GLBaseView compileShaderCode:fragmentShaderSource withType:GL_FRAGMENT_SHADER];

    _programHandle = glCreateProgram();
    glAttachShader(_programHandle, vertexShader);
    glAttachShader(_programHandle, fragmentShader);
    glLinkProgram(_programHandle);

    GLint linkSuccess;
    glGetProgramiv(_programHandle, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(_programHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetProgramiv ShaderIngoLog: %@", messageString);
        exit(1);
    }
    glUseProgram(_programHandle);
    glAttribute[ATTRIBUTE_POSITION] = glGetAttribLocation(_programHandle, "position");
    glAttribute[ATTRIBUTE_INPUT_TEXTURE_COORDINATE]  = glGetAttribLocation(_programHandle, "texturecoord");


//    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(90, CGRectGetWidth(self.bounds) * 1.0 / CGRectGetHeight(self.bounds), 0.01, 10);
//
//    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeLookAt(0, 0, 0,
//                                                      1, 0, 0,
//                                                      0, 1, 0);
//
//    glUniformMatrix4fv(uniforms[UNIFORM_PROJECTION_MARTRIX], 1, GL_FALSE, projectionMatrix.m);
//    glUniformMatrix3fv(uniforms[UNIFORM_MODELVIEW_MARTRIX], 1, GL_FALSE, modelViewMatrix.m);

    glEnableVertexAttribArray(glAttribute[ATTRIBUTE_POSITION]);
    glEnableVertexAttribArray(glAttribute[ATTRIBUTE_INPUT_TEXTURE_COORDINATE]);


    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

-(void)render{
    glViewport(0, 0, self.frame.size.width, self.frame.size.height);
    glClearColor(0, 0, 1, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE,0);

    [_context presentRenderbuffer:GL_RENDERBUFFER_OES];
}

- (void)renderImage:(UIImage *)image {
    _image = image;
    size_t width = CGImageGetWidth(image.CGImage);
    size_t height = CGImageGetHeight(image.CGImage);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *imageData = malloc( height * width * 4 );

    CGContextRef context = CGBitmapContextCreate(imageData,
                                                 width,
                                                 height,
                                                 8,
                                                 4 * width,
                                                 colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);

    CGColorSpaceRelease( colorSpace );
    CGContextClearRect( context, CGRectMake( 0, 0, width, height ) );
    CGContextTranslateCTM(context, 0, height);
    CGContextScaleCTM (context, 1.0,-1.0);
    CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), image.CGImage );
    CGContextRelease(context);

    glActiveTexture(GL_TEXTURE1);
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 (GLint)width,
                 (GLint)height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 imageData);
    free(imageData);
}

@end
