//
//  GLRGBView.m
//  GLGraph
//
//  Created by cfans on 2017/12/27.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLRGBView.h"
@import OpenGLES;


enum AttribEnum
{
    ATTRIB_VERTEX,
    ATTRIB_TEXTURE,
};

static NSString *vertexShaderSource = ShaderCode(
     attribute vec4 position;
     attribute vec2 texCoordIn;
     varying lowp vec2 texCoordOut;

     void main() {
         texCoordOut = texCoordIn;
         gl_Position = position;
     }
);

static NSString *fragmentShaderSource = ShaderCode(
       varying lowp vec2 texCoordOut;
       uniform sampler2D textureImage;
       void main() {
           gl_FragColor = texture2D(textureImage, texCoordOut);
       }
);

@interface GLRGBView(){
    int _width;
    int _height;
    GLuint _texture;
}
@end

@implementation GLRGBView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupGLView];
    }
    return self;
}

- (void)displayData:(void *)data width:(int)w height:(int)h{
    @synchronized(self){
        if (w != _width || h != _height)
        {
            [self setVideoSize:w height:h];
        }

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        [self render];
    }
}


-(void)render{
    static GLushort indices[] = {0, 1, 2, 0, 2, 3};
    glClear(GL_COLOR_BUFFER_BIT);
    if (_isDual) {
        glViewport(0, _startY, self.bounds.size.width/2, _offsetY);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
        glViewport(self.bounds.size.width/2, _startY, self.bounds.size.width/2, _offsetY);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
    }else{
        glViewport(0, 0, self.bounds.size.width, self.bounds.size.height);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
    }
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)setupGLView{
    [super setupGLView];
    [self compileShaders];
}

-(void)setupRGBTexture:(int)w height:(int)h{
    if(_texture){
        glDeleteTextures(1, &_texture);
    }

    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);        //把纹理像素映射到帧缓存中
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    static GLfloat vVertices[] = {
        // 解决纹理这样贴反的问题
        -1.0f,  1.0f, 0.0f,  // Position 0
        0.0f,  0.0f,        // TexCoord 0

        -1.0f, -1.0f, 0.0f,  // Position 1
        0.0f,  1.0f,        // TexCoord 1

        1.0f, -1.0f, 0.0f,  // Position 2
        1.0f,  1.0f,   // TexCoord 2


        1.0f,  1.0f, 0.0f,  // Position 3
        1.0f,  0.0f,   // TexCoord 3

    };
    // Load the vertex position
    glVertexAttribPointer (ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), vVertices);
    glEnableVertexAttribArray (ATTRIB_VERTEX);
    // Load the texture coordinate
    glVertexAttribPointer (ATTRIB_TEXTURE, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &vVertices[3]);
    glEnableVertexAttribArray (ATTRIB_TEXTURE);
}

-(void)compileShaders{
    GLuint vertexShader = [GLBaseView compileShaderCode:vertexShaderSource withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [GLBaseView compileShaderCode:fragmentShaderSource withType:GL_FRAGMENT_SHADER];

    _program = glCreateProgram();

    glAttachShader(_program, vertexShader);
    glAttachShader(_program, fragmentShader);

    /**
     绑定需要在link之前
     */
    glBindAttribLocation(_program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(_program, ATTRIB_TEXTURE, "texCoordIn");

    glLinkProgram(_program);

    GLint linkSuccess;
    glGetProgramiv(_program, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(_program, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetProgramiv ShaderIngoLog: %@", messageString);
        exit(1);
    }

    glUseProgram(_program);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

- (void)setVideoSize:(int)width height:(int)height
{
    _width = width;
    _height = height;
    _offsetY = self.bounds.size.width/2*_height/_width;
    _startY = (self.bounds.size.height - _offsetY)/2;
    [self setupRGBTexture:width height:height];
}

@end
