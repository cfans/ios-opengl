//
//  GLEyeView.h
//  GLGraph
//
//  Created by cfans on 2017/12/12.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLBaseView.h"

@interface GLEyeView : GLBaseView
/** 图片 */
@property (nonatomic, weak) UIImage * image;

@end
