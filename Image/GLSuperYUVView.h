//
//  GLYUVView.h
//  GLGraph
//
//  Created by cfans on 2017/12/27.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLBaseView.h"

@interface GLSuperYUVView : GLBaseView

/** 图片HSV灰度调节,范围：0-360 默认0 */
@property (nonatomic, assign) CGFloat  hue;
/** 图片HSV饱和度调节，范围：0-1 默认 1 */
@property (nonatomic, assign) CGFloat  saturation;
/** 图片HSV亮度调节，范围：0-1 默认1 */
@property (nonatomic, assign) CGFloat  value;
/** 图片对比度调节，范围：0-1 默认0 */
@property (nonatomic, assign) CGFloat  contrast;


@end
