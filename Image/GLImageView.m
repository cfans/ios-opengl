//
//  GLImageView.h
//  GLGraph
//
//  Created by cfans on 2017/12/8.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLImageView.h"
@import OpenGLES;

#define ToString(x) #x
#define ShaderCode(text) @ ToString(text)

static NSString *vertexShaderSource = ShaderCode(
     attribute vec4 position;
     attribute vec2 texCoordIn;
     varying lowp vec2 texCoordOut;

     void main() {
         texCoordOut = texCoordIn;
         gl_Position = position;
     }
);

static NSString *fragmentShaderSource = ShaderCode(
   uniform sampler2D textureImage;
   varying lowp vec2 texCoordOut;

   void main() {
       mediump vec4 RGBA;
       RGBA = texture2D(textureImage, texCoordOut);
       gl_FragColor = RGBA ;
   }
);

static NSString *fragmentSaturationShader = ShaderCode(
    uniform sampler2D textureImage;
    varying lowp vec2 texCoordOut;

    uniform lowp float saturation;

    const lowp vec3 warmFilter = vec3(0.93, 0.54, 0.0);
    const mediump mat3 RGBtoYIQ = mat3(0.299, 0.587, 0.114, 0.596, -0.274, -0.322, 0.212, -0.523, 0.311);
    const mediump mat3 YIQtoRGB = mat3(1.0, 0.956, 0.621, 1.0, -0.272, -0.647, 1.0, -1.105, 1.702);
    const mediump vec3 luminanceWeighting = vec3(0.2125, 0.7154, 0.0721);

    void main()
    {
        lowp vec4 source = texture2D(textureImage, texCoordOut);
        lowp float luminance = dot(source.rgb, luminanceWeighting);
        lowp vec3 greyScaleColor = vec3(luminance);
        gl_FragColor = vec4(mix(greyScaleColor, source.rgb, saturation), source.w);
    }
);


static NSString *fragmentHSVShader = ShaderCode(
   precision mediump float;

   uniform sampler2D textureImage;
   varying lowp vec2 texCoordOut;

   uniform float hue;
   uniform float saturation;
   uniform float value;
   uniform float contrast;

   vec3 rgbtohsv(vec3 rgb){
        float R = rgb.x;
        float G = rgb.y;
        float B = rgb.z;
        vec3 hsv;
        float max1 = max(R, max(G, B));
        float min1 = min(R, min(G, B));
        if (R == max1)
        {
            hsv.x = (G - B) / (max1 - min1);
        }
        if (G == max1)
        {
            hsv.x = 2.0 + (B - R) / (max1 - min1);
        }
        if (B == max1)
        {
            hsv.x = 4.0 + (R - G) / (max1 - min1);
        }
        hsv.x = hsv.x * 60.0;
        if (hsv.x  < 0.0)
        {
            hsv.x = hsv.x + 360.0;
        }
        hsv.z = max1;
        hsv.y = (max1 - min1) / max1;
        return hsv;
    }
    vec3 hsvtorgb(vec3 hsv)
    {
        float R;
        float G;
        float B;
        if (hsv.y == 0.0)
        {
            R = G = B = hsv.z;
        }
        else
        {
            hsv.x = hsv.x / 60.0;
            int i = int(hsv.x);
            float f = hsv.x - float(i);
            float a = hsv.z * (1.0 - hsv.y);
            float b = hsv.z * (1.0 - hsv.y * f);
            float c = hsv.z * (1.0 - hsv.y * (1.0 - f));
            if (i == 0)
            {
                R = hsv.z;
                G = c;
                B = a;
            }
            else if (i == 1)
            {
                R = b;
                G = hsv.z;
                B = a;
            }
            else if (i == 2)
            {
                R = a;
                G = hsv.z;
                B = c;
            }
            else if (i == 3)
            {
                R = a;
                G = b;
                B = hsv.z;
            }
            else if (i == 4)
            {
                R = c;
                G = a;
                B = hsv.z;
            }
            else
            {
                R = hsv.z;
                G = a;
                B = b;
            }
        }
        return vec3(R, G, B);
    }

   void main()
   {
       vec4 pixColor = texture2D(textureImage, texCoordOut);
       vec3 hsv;
       hsv.xyz = rgbtohsv(pixColor.rgb);
       hsv.x += hue;
       hsv.x = mod(hsv.x, 360.0);
       hsv.y *= saturation;
       hsv.z *= value;
       vec3 f_color = hsvtorgb(hsv);
       f_color = ((f_color - 0.5) * max(contrast+1.0, 0.0)) + 0.5;
       gl_FragColor = vec4(f_color, pixColor.a);
   }
);

typedef struct
{
    float position[4];
    float textureCoordinate[2];
} Vertex;

enum
{
    ATTRIBUTE_POSITION = 0,
    ATTRIBUTE_INPUT_TEXTURE_COORDINATE,
    TEMP_ATTRIBUTE_POSITION,
    TEMP_ATTRIBUTE_INPUT_TEXTURE_COORDINATE,
    NUM_ATTRIBUTES
};
GLint glAttributes[NUM_ATTRIBUTES];

enum
{
    UNIFORM_INPUT_IMAGE_TEXTURE = 0,
    TEMP_UNIFORM_INPUT_IMAGE_TEXTURE,
    UNIFORM_HUE,
    UNIFORM_SATURATION,
    UNIFORM_VALUE,
    UNIFORM_CONTRAST,
    NUM_UNIFORMS
};
GLint glViewUniforms[NUM_UNIFORMS];

@interface GLImageView(){
    CAEAGLLayer *_eaglLayer;
    EAGLContext *_context;
    GLuint       _frameBuffer;
    GLuint       _renderBuffer;

    GLuint       _texture;

    GLuint       _tempFramebuffer;
    GLuint       _tempTexture;
    GLuint       _tempRenderBuffer;

    GLuint       indexBuffer;
    GLuint       vertexBuffer;
    GLuint       _programHandle;
    GLuint       _tempProgramHandle;
}
@end


@implementation GLImageView


#pragma mark - Life Cycle
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initGLView];
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initGLView];
    }
    return self;
}

- (void)dealloc {
    if (_frameBuffer) {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    if (_renderBuffer) {
        glDeleteRenderbuffers(1, &_renderBuffer);
        _renderBuffer = 0;
    }
    _context = nil;
}


-(void)initGLView{
    _hue = 0;
    _value = 1;
    _saturation = 1;
    _contrast = 0;
    [self setupLayer];// 创建Layout
    [self setupContext];//创建GL上下文
    [self setupRenderBuffer]; //创建渲染Buffer
    [self setupFrameBuffer]; //创建帧Buffer
    [self compileShaders]; //编译Shader
    [self setupVBOs];

}

-(void)setImage:(UIImage *)image{
    [self renderImage:image];
    [self render];

}
-(void)setSaturation:(CGFloat)saturation{
    _saturation = saturation;
    [self render];
}
-(void)setHue:(CGFloat)hue{
    _hue = hue;
    [self render];
}
-(void)setValue:(CGFloat)value{
    _value = value;
    [self render];
}

-(void)setContrast:(CGFloat)contrast{
    _contrast = contrast;
    [self render];
}
+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)setupLayer {
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
}

- (void)setupContext {
    //创建一个OpenGLES 2.0接口的context
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    //将其设置为current context
    NSAssert(_context && [EAGLContext setCurrentContext:_context], @"初始化GL环境失败");
}

- (void)setupRenderBuffer {
    // 释放旧的 renderbuffer
    if (_renderBuffer) {
        glDeleteRenderbuffers(1, &_renderBuffer);
        _renderBuffer = 0;
    }
    //创建一个render buffer对象
    glGenRenderbuffers(1, &_renderBuffer);
    //绑定到GL_RENDERBUFFER目标上
    glBindRenderbuffer(GL_RENDERBUFFER, _renderBuffer);
    //为render buffer分配存储空间
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
}

- (void)setupFrameBuffer {
    // 释放旧的 framebuffer
    if (_frameBuffer) {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    // 生成 framebuffer ( framebuffer = 画布 )
    glGenFramebuffers(1, &_frameBuffer);
    // 绑定到GL_FRAMEBUFFER目标上
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    //将之前创建的render buffer附着到frame buffer作为其logical buffer
    //GL_COLOR_ATTACHMENT0指定第一个颜色缓冲区附着点
    // framebuffer 不对绘制的内容做存储, 所以这一步是将 framebuffer 绑定到 renderbuffer ( 绘制的结果就存在 renderbuffer )
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, _renderBuffer);
}

- (void)setupVBOs {
    static const Vertex vertices[] =
    {
        { .position = { 1.0, -1.0,0, 1}, .textureCoordinate = { 1.0, 0.0 } },
        { .position = {  1.0, 1.0,0, 1 }, .textureCoordinate = { 1.0, 1.0 } },
        { .position = { -1.0,  1.0,0, 1 }, .textureCoordinate = { 0.0, 1.0 } },
        { .position = {  -1.0,  -1.0,0, 1 }, .textureCoordinate = { 0, 0 } }
    };

    static const GLubyte index[] =
    {
        0, 1, 2,
        2, 3, 0
    };

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


    glGenBuffers(1, &indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index,GL_STATIC_DRAW);

    glVertexAttribPointer(glAttributes[ATTRIBUTE_POSITION], 4, GL_FLOAT, GL_FALSE,  sizeof(Vertex), (void*)0);

    glVertexAttribPointer(glAttributes[ATTRIBUTE_INPUT_TEXTURE_COORDINATE], 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(4 * sizeof(float)));

}

- (void)compileShaders {
    GLuint vertexShader = [self compileShaderCode:vertexShaderSource withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [self compileShaderCode:fragmentHSVShader withType:GL_FRAGMENT_SHADER];

    _programHandle = glCreateProgram();
    glAttachShader(_programHandle, vertexShader);
    glAttachShader(_programHandle, fragmentShader);
    glLinkProgram(_programHandle);

    GLint linkSuccess;
    glGetProgramiv(_programHandle, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(_programHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetProgramiv ShaderIngoLog: %@", messageString);
        exit(1);
    }
    glUseProgram(_programHandle);
    glAttributes[ATTRIBUTE_POSITION] = glGetAttribLocation(_programHandle, "position");
    glAttributes[ATTRIBUTE_INPUT_TEXTURE_COORDINATE]  = glGetAttribLocation(_programHandle, "texCoordIn");
    glViewUniforms[UNIFORM_SATURATION] = glGetUniformLocation(_programHandle, "saturation");
    glViewUniforms[UNIFORM_HUE] = glGetUniformLocation(_programHandle, "hue");
    glViewUniforms[UNIFORM_VALUE] = glGetUniformLocation(_programHandle, "value");
    glViewUniforms[UNIFORM_CONTRAST] = glGetUniformLocation(_programHandle, "contrast");

    glEnableVertexAttribArray(glAttributes[ATTRIBUTE_POSITION]);
    glEnableVertexAttribArray(glAttributes[ATTRIBUTE_INPUT_TEXTURE_COORDINATE]);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

-(void)render{

    glViewport(0, 0, self.frame.size.width, self.frame.size.height);
    glClearColor(0, 0, 1, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glUniform1f(glViewUniforms[UNIFORM_HUE], _hue);
    glUniform1f(glViewUniforms[UNIFORM_VALUE], _value);
    glUniform1f(glViewUniforms[UNIFORM_CONTRAST], _contrast);
    glUniform1f(glViewUniforms[UNIFORM_SATURATION], _saturation);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE,0);

    [_context presentRenderbuffer:GL_RENDERBUFFER_OES];

}

- (void)renderImage:(UIImage *)image {
    _image = image;
    size_t width = CGImageGetWidth(image.CGImage);
    size_t height = CGImageGetHeight(image.CGImage);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    void *imageData = malloc( height * width * 4 );

    CGContextRef context = CGBitmapContextCreate(imageData,
                                                 width,
                                                 height,
                                                 8,
                                                 4 * width,
                                                 colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);

    CGColorSpaceRelease( colorSpace );
    CGContextClearRect( context, CGRectMake( 0, 0, width, height ) );
    CGContextTranslateCTM(context, 0, height);
    CGContextScaleCTM (context, 1.0,-1.0);
    CGContextDrawImage( context, CGRectMake( 0, 0, width, height ), image.CGImage );
    CGContextRelease(context);

    glActiveTexture(GL_TEXTURE1);
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 GL_RGBA,
                 (GLint)width,
                 (GLint)height,
                 0,
                 GL_RGBA,
                 GL_UNSIGNED_BYTE,
                 imageData);
    free(imageData);
}

- (GLuint)compileShaderCode:(NSString *)shaderCode withType:(GLenum)shaderType {
    GLuint shaderHandle = glCreateShader(shaderType);
    const char* shaderStringUFT8 = [shaderCode UTF8String];
    int shaderStringLength = (int)[shaderCode length];
    glShaderSource(shaderHandle, 1, &shaderStringUFT8, &shaderStringLength);
    glCompileShader(shaderHandle);
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetShaderiv ShaderIngoLog: %@", messageString);
        exit(1);
    }
    return shaderHandle;
}


-(NSString *)loadShaderCode:(NSString *)shaderName{
    NSString *shaderPath = [[NSBundle mainBundle] pathForResource:shaderName ofType:nil];
    NSError *error;
    NSString *shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"load Shader Code error=%@,shader name=%@",error,shaderName);
    }
    return shaderString;
}
@end
