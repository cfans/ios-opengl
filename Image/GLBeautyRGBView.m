//
//  GLSuperRGBView.m
//  GLGraph
//
//  Created by cfans on 2018/1/10.
//  Copyright © 2018年 cfans. All rights reserved.
//

#import "GLBeautyRGBView.h"
@import OpenGLES;


static NSString *vertexShaderSource = ShaderCode(
     attribute vec4 position;
     attribute vec4 inputTextureCoordinate;

     const int GAUSSIAN_SAMPLES = 9;

     uniform float texelWidthOffset;
     uniform float texelHeightOffset;

     varying vec2 textureCoordinate;
     varying vec2 blurCoordinates[GAUSSIAN_SAMPLES];

     void main()
     {
         gl_Position = position;
         textureCoordinate = inputTextureCoordinate.xy;

         // Calculate the positions for the blur
         int multiplier = 0;
         vec2 blurStep;
         vec2 singleStepOffset = vec2(texelWidthOffset, texelHeightOffset);

         for (int i = 0; i < GAUSSIAN_SAMPLES; i++)
         {
             multiplier = (i - ((GAUSSIAN_SAMPLES - 1) / 2));
             // Blur in x (horizontal)
             blurStep = float(multiplier) * singleStepOffset;
             blurCoordinates[i] = inputTextureCoordinate.xy + blurStep;
         }
     }
);

static NSString *fragmentShaderSource = ShaderCode(

   uniform sampler2D inputImageTexture;

   const lowp int GAUSSIAN_SAMPLES = 9;

   varying highp vec2 textureCoordinate;
   varying highp vec2 blurCoordinates[GAUSSIAN_SAMPLES];

   uniform mediump float distanceNormalizationFactor;
   uniform lowp float brightness;

   void main()
   {
       lowp vec4 centralColor;
       lowp float gaussianWeightTotal;
       lowp vec4 sum;
       lowp vec4 sampleColor;
       lowp float distanceFromCentralColor;
       lowp float gaussianWeight;
       lowp vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);

       centralColor = texture2D(inputImageTexture, blurCoordinates[4]);
       gaussianWeightTotal = 0.18;
       sum = centralColor * 0.18;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[0]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.05 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[1]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.09 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[2]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.12 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[3]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.15 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[5]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.15 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[6]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.12 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[7]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.09 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       sampleColor = texture2D(inputImageTexture, blurCoordinates[8]);
       distanceFromCentralColor = min(distance(centralColor, sampleColor) * distanceNormalizationFactor, 1.0);
       gaussianWeight = 0.05 * (1.0 - distanceFromCentralColor);
       gaussianWeightTotal += gaussianWeight;
       sum += sampleColor * gaussianWeight;

       gl_FragColor = sum / gaussianWeightTotal;
       gl_FragColor = vec4((gl_FragColor.rgb + vec3(brightness)), gl_FragColor.w);
   }
);

enum AttribEnum
{
    ATTRIB_VERTEX,
    ATTRIB_TEXTURE,

    UNIFORM_VALUE,
    UNIFORM_FACTOR,
    UNIFORM_WIDTH_OFFSET,
    UNIFORM_HEIGHT_OFFSET,
    NUM_UNIFORMS
};
static GLint glViewUniforms[NUM_UNIFORMS];


@interface GLBeautyRGBView(){
    int _width;
    int _height;

    GLuint _texture;
    GLint _samplerLoc;

}
@end

@implementation GLBeautyRGBView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupGLView];
    }
    return self;
}

- (void)displayData:(void *)data width:(int)w height:(int)h{
    @synchronized(self)
    {
        if (w != _width || h != _height)
        {
            [self setVideoSize:w height:h];
        }
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        [self render];
    }
}


-(void)setDistanceNormalizationFactor:(CGFloat)distanceNormalizationFactor{
    _distanceNormalizationFactor = distanceNormalizationFactor;
    [self render];
}

-(void)setValue:(CGFloat)value{
    _value = value;
    [self render];
}

-(void)render{
    glClear(GL_COLOR_BUFFER_BIT);
    if (_isDual) {
        glViewport(0, _startY, self.bounds.size.width/2, _offsetY);
        [self doRender];
        glViewport(self.bounds.size.width/2, _startY, self.bounds.size.width/2, _offsetY);
        [self doRender];
    }else{
        glViewport(0, 0, self.bounds.size.width, self.bounds.size.height);
        [self doRender];
    }
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)doRender{

    NSLog(@"%f,%f,%d,%d,%f",_value,_distanceNormalizationFactor,_width,_height,_texelWidthOffset);

    glUniform1f(glViewUniforms[UNIFORM_VALUE], _value);
    glUniform1f(glViewUniforms[UNIFORM_WIDTH_OFFSET], _texelWidthOffset/_width);
    glUniform1f(glViewUniforms[UNIFORM_HEIGHT_OFFSET], _texelHeightOffset/_height);
    glUniform1f(glViewUniforms[UNIFORM_FACTOR], _distanceNormalizationFactor);

    static GLushort indices[] = {0, 1, 2, 0, 2, 3};
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, indices);
}

-(void)setupGLView{
    _value = 1;
    _distanceNormalizationFactor = 4.0;
    _texelWidthOffset = 1.0;
    _texelHeightOffset = 0.0;
    [super setupGLView];
    [self compileShaders];
}

-(void)setupRGBTexture:(int)w height:(int)h{
    if (_texture) {
        glDeleteTextures(1, &_texture);
        _texture = 0;
    }
    glGenTextures(1, &_texture);
    glBindTexture(GL_TEXTURE_2D, _texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);//把纹理像素映射到帧缓存中
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    static GLfloat vVertices[] = {
        // 解决纹理这样贴反的问题
        -1.0f,  1.0f, 0.0f,  // Position 0
        0.0f,  0.0f,        // TexCoord 0

        -1.0f, -1.0f, 0.0f,  // Position 1
        0.0f,  1.0f,        // TexCoord 1

        1.0f, -1.0f, 0.0f,  // Position 2
        1.0f,  1.0f,   // TexCoord 2


        1.0f,  1.0f, 0.0f,  // Position 3
        1.0f,  0.0f,   // TexCoord 3

    };

    // Load the vertex position
    glVertexAttribPointer (ATTRIB_VERTEX, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), vVertices);
    glEnableVertexAttribArray (ATTRIB_VERTEX);
    // Load the texture coordinate
    glVertexAttribPointer (ATTRIB_TEXTURE, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), &vVertices[3]);
    glEnableVertexAttribArray (ATTRIB_TEXTURE);
}

-(void)compileShaders{
    GLuint vertexShader = [GLBaseView compileShaderCode:vertexShaderSource withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [GLBaseView compileShaderCode:fragmentShaderSource withType:GL_FRAGMENT_SHADER];
    
    _program = glCreateProgram();

    glAttachShader(_program, vertexShader);
    glAttachShader(_program, fragmentShader);

    /**
     绑定需要在link之前
     */
    glBindAttribLocation(_program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(_program, ATTRIB_TEXTURE, "inputTextureCoordinate");

    glLinkProgram(_program);

    GLint linkSuccess;
    glGetProgramiv(_program, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(_program, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetProgramiv ShaderIngoLog: %@", messageString);
        exit(1);
    }

    glUseProgram(_program);

    glViewUniforms[UNIFORM_FACTOR]= glGetUniformLocation(_program,"distanceNormalizationFactor");
    glViewUniforms[UNIFORM_WIDTH_OFFSET]  = glGetUniformLocation(_program, "texelWidthOffset");
    glViewUniforms[UNIFORM_HEIGHT_OFFSET]  = glGetUniformLocation(_program, "texelHeightOffset");
    glViewUniforms[UNIFORM_VALUE] = glGetUniformLocation(_program, "brightness");

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}


- (void)setVideoSize:(int)width height:(int)height
{
    _width = width;
    _height = height;
    _offsetY = self.bounds.size.width/2*_height/_width;
    _startY = (self.bounds.size.height - _offsetY)/2;
    [self setupRGBTexture:width height:height];
}

@end
