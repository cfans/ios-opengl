//
//  GLSuperRGBView.h
//  GLGraph
//
//  Created by cfans on 2018/1/10.
//  Copyright © 2018年 cfans. All rights reserved.
//

#import "GLBaseView.h"

@interface GLBeautyRGBView : GLBaseView

/** 亮度调节，范围：0-1 默认1 */
@property (nonatomic, assign) CGFloat  value;
// Exposure ranges from 0.0 to 16.0, with 8.0 as the normal level
@property(nonatomic, assign) CGFloat distanceNormalizationFactor;

@property(nonatomic, assign) CGFloat texelWidthOffset;
@property(nonatomic, assign) CGFloat texelHeightOffset;

@end
