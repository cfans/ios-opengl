//
//  GLYUVView.m
//  GLGraph
//
//  Created by cfans on 2017/12/27.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLYUVView.h"
@import OpenGLES;


enum AttribEnum
{
    ATTRIB_VERTEX,
    ATTRIB_TEXTURE,
};

enum TextureYUVType
{
    TEXY = 0,
    TEXU,
    TEXV,
};


static NSString *vertexShaderSource = ShaderCode(
     attribute vec4 position;
     attribute vec2 texCoordIn;
     varying lowp vec2 texCoordOut;

     void main() {
         texCoordOut = texCoordIn;
         gl_Position = position;
     }
);

static NSString *fragmentShaderSource = ShaderCode(
    varying lowp vec2 texCoordOut;
    uniform sampler2D SamplerY;
    uniform sampler2D SamplerU;
    uniform sampler2D SamplerV;

    void main() {
        mediump vec3 yuv;
        lowp vec3 rgb;
        yuv.x = texture2D(SamplerY, texCoordOut).r;
        yuv.y = texture2D(SamplerU, texCoordOut).r - 0.5;
        yuv.z = texture2D(SamplerV, texCoordOut).r - 0.5;
        rgb = mat3( 1,       1,         1,
                   0,       -0.39465,  2.03211,
                   1.13983, -0.58060,  0) * yuv;
        gl_FragColor = vec4(rgb, 1);
    }
);


@interface GLYUVView(){
    /**
     YUV纹理数组
     */
    GLuint _textureYUV[3];
    int _width;
    int _height;
}

@end

@implementation GLYUVView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupGLView];
    }
    return self;
}

- (void)displayData:(void *)data width:(int)w height:(int)h{

    @synchronized(self){
        if (w != _width || h != _height)
        {
            [self setVideoSize:w height:h];
        }
        
        [EAGLContext setCurrentContext:_context];
        glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXY]);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w, h, GL_RED_EXT, GL_UNSIGNED_BYTE, data);

        glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXU]);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w/2, h/2, GL_RED_EXT, GL_UNSIGNED_BYTE, data + w * h);

        glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXV]);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, w/2, h/2, GL_RED_EXT, GL_UNSIGNED_BYTE, data + w * h * 5 / 4);
        [self render];
    }
}

- (void)setVideoSize:(int)width height:(int)height
{

    void *blackData = malloc(width * height * 1.5);
    if(blackData){
        memset(blackData, 0x0, width * height * 1.5);
        _width = width;
        _height = height;
        _offsetY = self.bounds.size.width/2*_height/_width;
        _startY = (self.bounds.size.height - _offsetY)/2;
        [EAGLContext setCurrentContext:_context];
        glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXY]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED_EXT, width, height, 0, GL_RED_EXT, GL_UNSIGNED_BYTE, blackData);
        glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXU]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED_EXT, width/2, height/2, 0, GL_RED_EXT, GL_UNSIGNED_BYTE, blackData + width * height);

        glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXV]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED_EXT, width/2, height/2, 0, GL_RED_EXT, GL_UNSIGNED_BYTE, blackData + width * height * 5 / 4);
        free(blackData);
    }
}

-(void)render{
    glClear(GL_COLOR_BUFFER_BIT);
    if (_isDual) {
        glViewport(0, _startY, self.bounds.size.width/2, _offsetY);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glViewport(self.bounds.size.width/2, _startY, self.bounds.size.width/2, _offsetY);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }else{
        glViewport(0, 0, self.bounds.size.width, self.bounds.size.height);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)setupGLView{
    [super setupGLView];
    [self compileShaders];
    [self setupYUVTexture];
}

- (void)setupYUVTexture{

    if (_textureYUV[TEXY])
    {
        glDeleteTextures(3, _textureYUV);
    }
    glGenTextures(3, _textureYUV);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXY]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXU]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, _textureYUV[TEXV]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


    GLuint textureUniformY = glGetUniformLocation(_program, "SamplerY");
    GLuint textureUniformU = glGetUniformLocation(_program, "SamplerU");
    GLuint textureUniformV = glGetUniformLocation(_program, "SamplerV");

    glUniform1i(textureUniformY, TEXY);
    glUniform1i(textureUniformU, TEXU);
    glUniform1i(textureUniformV, TEXV);

    static const GLfloat squareVertices[] = {
        -1.0f, -1.0f,
        1.0f, -1.0f,
        -1.0f,  1.0f,
        1.0f,  1.0f,
    };

    static const GLfloat coordVertices[] = {
        0.0f, 1.0f,
        1.0f, 1.0f,
        0.0f,  0.0f,
        1.0f,  0.0f,
    };
    // Update attribute values
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, 0, 0, squareVertices);
    glEnableVertexAttribArray(ATTRIB_VERTEX);


    glVertexAttribPointer(ATTRIB_TEXTURE, 2, GL_FLOAT, 0, 0, coordVertices);
    glEnableVertexAttribArray(ATTRIB_TEXTURE);
    
}


- (void)compileShaders {
    GLuint vertexShader = [GLBaseView compileShaderCode:vertexShaderSource withType:GL_VERTEX_SHADER];
    GLuint fragmentShader = [GLBaseView compileShaderCode:fragmentShaderSource withType:GL_FRAGMENT_SHADER];

    _program = glCreateProgram();

    glAttachShader(_program, vertexShader);
    glAttachShader(_program, fragmentShader);

    /**
     绑定需要在link之前
     */
    glBindAttribLocation(_program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(_program, ATTRIB_TEXTURE, "texCoordIn");

    glLinkProgram(_program);

    GLint linkSuccess;
    glGetProgramiv(_program, GL_LINK_STATUS, &linkSuccess);
    if (linkSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(_program, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetProgramiv ShaderIngoLog: %@", messageString);
        exit(1);
    }
    glUseProgram(_program);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}



@end
