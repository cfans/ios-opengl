//
//  GLTriangle.m
//  GLGraph
//
//  Created by cfans on 2017/12/8.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLRectangle.h"
@import OpenGLES;


#define ToString(x) #x
#define ShaderCode(text) @ ToString(text)

static NSString *vertexShaderSource = ShaderCode(
    attribute vec4 position;
    void main(void) {
        gl_Position = position;
    }
);

static NSString *fragmentShaderSource = ShaderCode(
     void main(void) {
         gl_FragColor = vec4(0, 1, 0, 1);
     }
);


@interface GLRectangle(){
    CAEAGLLayer *_eaglLayer;
    EAGLContext *_context;
    GLuint       _frameBuffer;
    GLuint       _renderBuffer;


    GLuint  _program;
    unsigned int VAO[2];

}
@end

@implementation GLRectangle

#pragma mark - Life Cycle
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initGLView];
}


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initGLView];
    }
    return self;
}

- (void)dealloc {
    if (_frameBuffer) {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    if (_renderBuffer) {
        glDeleteRenderbuffers(1, &_renderBuffer);
        _renderBuffer = 0;
    }
    _context = nil;
}

-(void)initGLView{
    [self setupLayer];// 创建Layout
    [self setupContext];//创建GL上下文
    [self setupRenderBuffer]; //创建渲染Buffer
    [self setupFrameBuffer]; //创建帧Buffer
    [self setupProgram]; //创建program
    [self setupVertextData];//加载顶点数据
    [self render];
}

+ (Class)layerClass {
    return [CAEAGLLayer class];
}

- (void)setupLayer {
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;
//    _eaglLayer.drawableProperties = @{kEAGLDrawablePropertyRetainedBacking: @YES,
//                                      kEAGLDrawablePropertyColorFormat: kEAGLColorFormatRGBA8};
}

- (void)setupContext {
    //创建一个OpenGLES 2.0接口的context
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    //将其设置为current context
    NSAssert(_context && [EAGLContext setCurrentContext:_context], @"初始化GL环境失败");
}

- (void)setupRenderBuffer {
    // 释放旧的 renderbuffer
    if (_renderBuffer) {
        glDeleteRenderbuffers(1, &_renderBuffer);
        _renderBuffer = 0;
    }
    //创建一个render buffer对象
    glGenRenderbuffers(1, &_renderBuffer);
    //绑定到GL_RENDERBUFFER目标上
    glBindRenderbuffer(GL_RENDERBUFFER, _renderBuffer);

}

- (void)setupFrameBuffer {
    // 释放旧的 framebuffer
    if (_frameBuffer) {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    // 生成 framebuffer ( framebuffer = 画布 )
    glGenFramebuffers(1, &_frameBuffer);
    // 绑定到GL_FRAMEBUFFER目标上
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    //将之前创建的render buffer附着到frame buffer作为其logical buffer
    //GL_COLOR_ATTACHMENT0指定第一个颜色缓冲区附着点
    // framebuffer 不对绘制的内容做存储, 所以这一步是将 framebuffer 绑定到 renderbuffer ( 绘制的结果就存在 renderbuffer )
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, _renderBuffer);

    //为render buffer分配存储空间
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
}

-(void)setupProgram{
    //编译Vertext代码得到对应的ID
    GLuint _vertexShader = [self compileShaderCode:vertexShaderSource withType:GL_VERTEX_SHADER];
    //编译Fragment代码得到对应的ID
    GLuint _fragmentShader = [self compileShaderCode:fragmentShaderSource withType:GL_FRAGMENT_SHADER];
    //创建program并与Vertex Shader和Fragment Shader关联
    _program = glCreateProgram();
    glAttachShader(_program, _vertexShader);
    glAttachShader(_program, _fragmentShader);

    //链接program
    glLinkProgram(_program);

    //开始使用program
    glUseProgram(_program);

    glDeleteShader(_vertexShader);
    glDeleteShader(_fragmentShader);
}

- (void)setupVertextData {
    GLfloat vertexData[] =
    {
        0.5, 0.5, //右上
        -0.5, 0.5, //左上
        -0.5, -0.5, //左下

        -0.5, -0.5, //左下
        0.5, -0.5, //右下
        0.5, 0.5, //左上
    };

    //顶点数据缓存
    GLuint buffer;
    //创建buffer
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), NULL);
}

-(void)render{

    glViewport(0, 0, self.frame.size.width, self.frame.size.height);
    glClearColor(0, 0, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

- (GLuint)compileShaderCode:(NSString *)shaderCode withType:(GLenum)shaderType {
    GLuint shaderHandle = glCreateShader(shaderType);
    const char* shaderStringUFT8 = [shaderCode UTF8String];
    int shaderStringLength = (int)[shaderCode length];
    glShaderSource(shaderHandle, 1, &shaderStringUFT8, &shaderStringLength);
    glCompileShader(shaderHandle);
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetShaderiv ShaderIngoLog: %@", messageString);
        exit(1);
    }
    return shaderHandle;
}

-(NSString *)loadShaderCode:(NSString *)shaderName{
    NSString *shaderPath = [[NSBundle mainBundle] pathForResource:shaderName ofType:nil];
    NSError *error;
    NSString *shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"load Shader Code error=%@,shader name=%@",error,shaderName);
    }
    return shaderString;
}
@end
