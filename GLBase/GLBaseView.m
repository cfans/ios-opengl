//
//  GLBaseView.m
//  GLGraph
//
//  Created by cfans on 2017/12/12.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "GLBaseView.h"
@import  OpenGLES;


@implementation GLBaseView


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"scale %f,%f",[[UIScreen mainScreen] scale],self.contentScaleFactor);
//        self.contentScaleFactor = [[UIScreen mainScreen] scale];
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
        eaglLayer.opaque = TRUE;
    }
    return self;
}


- (void)dealloc {
    if (_frameBuffer) {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    if (_renderBuffer) {
        glDeleteRenderbuffers(1, &_renderBuffer);
        _renderBuffer = 0;
    }
    _context = nil;
}


-(void)setupGLView{
    [self setupContext];//创建GL上下文
    [self setupRenderBuffer]; //创建渲染Buffer
    [self setupFrameBuffer]; //创建帧Buffer
    NSLog(@"super setupGLView");
}

+ (Class)layerClass {
    return [CAEAGLLayer class];
}
- (void)setupContext {
    //创建一个OpenGLES 2.0接口的context
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    //将其设置为current context
    NSAssert(_context && [EAGLContext setCurrentContext:_context], @"初始化GL环境失败");
}

- (void)setupRenderBuffer {
    glDisable(GL_DEPTH_TEST);

    // 释放旧的 renderbuffer
    if (_renderBuffer) {
        glDeleteRenderbuffers(1, &_renderBuffer);
        _renderBuffer = 0;
    }
    //创建一个render buffer对象
    glGenRenderbuffers(1, &_renderBuffer);
    //绑定到GL_RENDERBUFFER目标上
    glBindRenderbuffer(GL_RENDERBUFFER, _renderBuffer);
    //为render buffer分配存储空间
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];

    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &_eaglLayerWidth);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &_eaglLayerHeight);
    NSLog(@"eaglLayer: %d,%d",_eaglLayerWidth,_eaglLayerHeight);
}

- (void)setupFrameBuffer {
    // 释放旧的 framebuffer
    if (_frameBuffer) {
        glDeleteFramebuffers(1, &_frameBuffer);
        _frameBuffer = 0;
    }
    // 生成 framebuffer ( framebuffer = 画布 )
    glGenFramebuffers(1, &_frameBuffer);
    // 绑定到GL_FRAMEBUFFER目标上
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBuffer);
    //将之前创建的render buffer附着到frame buffer作为其logical buffer
    //GL_COLOR_ATTACHMENT0指定第一个颜色缓冲区附着点
    // framebuffer 不对绘制的内容做存储, 所以这一步是将 framebuffer 绑定到 renderbuffer ( 绘制的结果就存在 renderbuffer )
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, _renderBuffer);
}

-(void)render{
    //Do nothing
}

- (void)displayData:(void *)data width:(int)width height:(int)height{
    //Do nothing

}
- (void)setDualViewPort:(BOOL)dual{
    @synchronized(self){
        if (_isDual != dual) {
            _isDual = dual;
            [self render];
        }
    }
}
- (BOOL)isDual{
    return _isDual;
}

# pragma static util method
+ (GLuint)compileShaderCode:(NSString *)shaderCode withType:(GLenum)shaderType {
    GLuint shaderHandle = glCreateShader(shaderType);
    const char* shaderStringUFT8 = [shaderCode UTF8String];
    int shaderStringLength = (int)[shaderCode length];
    glShaderSource(shaderHandle, 1, &shaderStringUFT8, &shaderStringLength);
    glCompileShader(shaderHandle);
    GLint compileSuccess;
    glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &compileSuccess);
    if (compileSuccess == GL_FALSE) {
        GLchar messages[256];
        glGetShaderInfoLog(shaderHandle, sizeof(messages), 0, &messages[0]);
        NSString *messageString = [NSString stringWithUTF8String:messages];
        NSLog(@"glGetShaderiv ShaderIngoLog: %@", messageString);
        exit(1);
    }
    return shaderHandle;
}


+(NSString *)loadShaderCode:(NSString *)shaderName{
    NSString *shaderPath = [[NSBundle mainBundle] pathForResource:shaderName ofType:nil];
    NSError *error;
    NSString *shaderString = [NSString stringWithContentsOfFile:shaderPath encoding:NSUTF8StringEncoding error:&error];
    if (error) {
        NSLog(@"load Shader Code error=%@,shader name=%@",error,shaderName);
    }
    return shaderString;
}

@end
