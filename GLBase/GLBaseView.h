//
//  GLBaseView.h
//  GLGraph
//
//  Created by cfans on 2017/12/12.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ToString(x) #x
#define ShaderCode(text) @ ToString(text)


@interface GLBaseView : UIView{
    
    @protected
        EAGLContext * _context;
        GLuint        _frameBuffer;
        GLuint        _renderBuffer;
        GLuint  _program;

    // The pixel dimensions of the CAEAGLLayer.
        GLint _eaglLayerWidth;
        GLint _eaglLayerHeight;

        BOOL _isDual;
        int _offsetY;
        int _startY;
}

-(void)setupGLView;
-(void)render;


- (void)displayData:(void *)data width:(int)width height:(int)height;
- (void)setDualViewPort:(BOOL)dual;
- (BOOL)isDual;

+ (GLuint)compileShaderCode:(NSString *)shaderCode withType:(GLenum)shaderType;
+ (NSString *)loadShaderCode:(NSString *)shaderName;

@end
