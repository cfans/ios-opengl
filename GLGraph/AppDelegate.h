//
//  AppDelegate.h
//  GLTriangle
//
//  Created by cfans on 2017/12/8.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

