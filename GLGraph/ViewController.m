//
//  ViewController.m
//  GLTriangle
//
//  Created by cfans on 2017/12/8.
//  Copyright © 2017年 cfans. All rights reserved.
//

#import "ViewController.h"
#import "GLTriangle.h"
#import "GLRectangle.h"
#import "GLImageView.h"
#import "GLEyeView.h"
#import "GLYUVView.h"
#import "GLSuperYUVView.h"

#import "GLRGBView.h"

#import "GLBeautyRGBView.h"


#define screenW ([UIScreen mainScreen].bounds.size.width)
#define screenH ([UIScreen mainScreen].bounds.size.height)


@interface ViewController (){

//    GLImageView * _view;
    UISlider * _hueSlider;
    UISlider * _saturationSlider;
    UISlider * _valueSlider;
    UISlider * _constrastSlider;
//    GLSuperYUVView * _view;
     GLBeautyRGBView * _view;
}

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
//    GLTriangle * graphic = [[GLTriangle alloc] initWithFrame:self.view.frame];
//    UIView * graphic = [[GLRectangle alloc] initWithFrame:self.view.frame];

//   _graphic = [[GLImageView alloc] initWithFrame:self.view.frame];
//    UIImage * image =[UIImage imageNamed:@"Lena.png"];
//    _graphic.image = image;
//    [self.view addSubview:_graphic];

//    GLEyeView * view = [[GLEyeView alloc] initWithFrame:self.view.frame];
//    UIImage * image =[UIImage imageNamed:@"Lena.png"];
//    view.image = image;
//    [self.view addSubview:view];


    _view = [[GLBeautyRGBView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:_view];
//
//    [view displayYUV420pData:data.bytes width:512 height:512];

//    _view = [[GLSuperRGBView alloc] initWithFrame:self.view.frame];
//    [self.view addSubview:_view];


    CGRect rect = CGRectMake(0, 20, self.view.frame.size.width, 40);
    UILabel *  label = [[UILabel alloc] initWithFrame:rect];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"IOS,OpenGL Simple Graphic";
    [self.view addSubview:label];

    [self setupSlider];
}

-(void)setupSlider{

    CGRect rect = CGRectMake(screenW*0.3, screenH-40, screenW*0.68, 40);
    _constrastSlider = [self createSlider:rect name:@"Contrast:"];
    [self.view addSubview:_constrastSlider];

    rect.origin.y -= 40;
    _hueSlider = [self createSlider:rect name:@"Hue:"];
    _hueSlider.maximumValue = 240;
    [self.view addSubview:_hueSlider];

    rect.origin.y -= 40;
    _saturationSlider = [self createSlider:rect name:@"Saturation:"];
//    _saturationSlider.value = 1;
    _saturationSlider.maximumValue = 16.0;
    _saturationSlider.minimumValue = 0.0;
    _saturationSlider.value = 8;
    [self.view addSubview:_saturationSlider];

    rect.origin.y -= 40;
    _valueSlider = [self createSlider:rect name:@"Brightness:"];
    _valueSlider.maximumValue = 1.0;
    _valueSlider.minimumValue = -1.0;
    _valueSlider.value = 0;

    [self.view addSubview:_valueSlider];

    rect.origin.y -= 40;
    UIButton * reset = [[UIButton alloc]initWithFrame:rect];
    [reset setTitle:@"Reset" forState:UIControlStateNormal];
    [reset addTarget:self action:@selector(didReset) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reset];

    rect.origin.y -= 40;
   reset = [[UIButton alloc]initWithFrame:rect];
    [reset setTitle:@"Double" forState:UIControlStateNormal];
    [reset addTarget:self action:@selector(didDouble) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reset];

    rect.origin.y -= 40;
    reset = [[UIButton alloc]initWithFrame:rect];
    [reset setTitle:@"Single" forState:UIControlStateNormal];
    [reset addTarget:self action:@selector(didSingle) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reset];


    NSTimer * timer = [NSTimer timerWithTimeInterval:0.05f target:self selector:@selector(didReset) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:timer forMode:NSRunLoopCommonModes];

    [timer fire];

}
-(void)didDouble{
//    [_view setDualViewPort:YES];
}

-(void)didSingle{
//    [_view setDualViewPort:NO];
}


-(void)didReset{
//    _view.value = _valueSlider.value = 0;
//    _view.saturation = _saturationSlider.value = 8;
//    _view.hue = _hueSlider.value = 0;
//    _view.contrast = _constrastSlider.value = 0;
//    _graphic.image = [UIImage imageNamed:@"3.png"];

//    NSString * path = [[NSBundle mainBundle] pathForResource:@"Lena" ofType:@"rgb"];
    NSString * path = [[NSBundle mainBundle] pathForResource:@"640x360" ofType:@"rgb"];

    NSData * data = [NSData dataWithContentsOfFile:path];
    NSLog(@"length: %ld",[data length]);
    CFAbsoluteTime startTime =CFAbsoluteTimeGetCurrent();

//    _view.image = [ViewController rgb24ToImage:data.bytes width:1280 height:720];

    [_view displayData:data.bytes width:640 height:360];
//    [_view displayData:data.bytes width:512 height:512];

    CFAbsoluteTime linkTime = (CFAbsoluteTimeGetCurrent() - startTime);
    NSLog(@"using time  %f ms", linkTime*1000.0);

}

+(UIImage *)rgb24ToImage:(void *)rgb width:(int)width height:(int)height{
    CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, rgb, width*height*3, NULL);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef cgImage = CGImageCreate(width,
                                       height,
                                       8,
                                       24,
                                       width*3,
                                       colorSpace,
                                       kCGImageAlphaNone|kCGBitmapByteOrderDefault,
                                       provider,
                                       NULL,
                                       NO,
                                       kCGRenderingIntentDefault);
    UIImage *image = [[UIImage alloc]initWithCGImage:cgImage];
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(cgImage);
    CGDataProviderRelease(provider);
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-( UISlider *)createSlider:(CGRect)rect name:(NSString *)sliderName{


    UISlider * slider = [[UISlider alloc]initWithFrame:rect];
    slider.minimumValue = 0;
    slider.maximumValue = 1.0;
    [slider addTarget:self action:@selector(seekbarValueChanged:) forControlEvents:UIControlEventValueChanged];

    rect.origin.x = 2;
    rect.size.width =  screenW*0.3;
    UILabel * label = [[UILabel alloc] initWithFrame:rect];
    label.text = sliderName;
    [self.view addSubview:label];
    return slider;
}
-(void)seekbarValueChanged:(id)sender{

    if (sender == _saturationSlider) {
        _view.distanceNormalizationFactor = _saturationSlider.value;
    }
//    else if (sender == _hueSlider) {
//        _view.hue = _hueSlider.value;
//    }else if (sender == _constrastSlider) {
//        _view.contrast = _constrastSlider.value;
//    }
    else if (sender == _valueSlider) {
        _view.value = _valueSlider.value;
    }
//    else if (sender == _valueSlider) {
//        _view.exposure = _valueSlider.value;
//    }
}

@end
